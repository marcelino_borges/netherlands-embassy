﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryPanel : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadLevelSelectScene() {
        Debug.Log("Entrou no LoadScene()");
        SceneManager.LoadScene("LevelSelect", LoadSceneMode.Single);
    }

    public void TryAgainAgain() {
        Debug.Log("Entrou no LoadScene()");
        SceneManager.LoadScene("LevelSelect", LoadSceneMode.Single);
    }
}
