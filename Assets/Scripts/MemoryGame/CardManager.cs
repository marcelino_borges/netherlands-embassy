﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardManager : MonoBehaviour {
    public AudioClip song_part1;
    public MusicManager musicManager;

    private const float timeBeforePunchingMatchedCard = .2f;
    public int targetScore = 4, currentScore;
    public Card firstFlippedCard, secondFlippedCard, cardInBigPicture;
    public bool canPlayTheGame;
    public bool isGameWon = false;

    public Animator victoryPanelAnimator, pausePanelAnimator, tipClickPictureAnimator, bgPauseAnimator;
    public GameObject pauseButton;

    void Start() {
        canPlayTheGame = true;
        VerifyMandatoryReferences();

        musicManager = GameObject.FindGameObjectWithTag("musicManager").GetComponent<MusicManager>();
        SetMusic();
    }

    private void SetMusic() {
        song_part1 = Resources.Load<AudioClip>("Sounds/Songs/cards1");

        if (musicManager != null) {
            musicManager.SetSongs(new AudioClip[] { song_part1, song_part1 });
        }
    }

    private void VerifyMandatoryReferences() {
        if (victoryPanelAnimator == null) {
            throw new MissingReferenceException("Colocar a referência do objeto do painel de vitória!");
        }

        if (pausePanelAnimator == null) {
            throw new MissingReferenceException("Colocar a referência do objeto do painel de pausa!");
        }

        if (tipClickPictureAnimator == null) {
            throw new MissingReferenceException("Colocar a referência do objeto da dica!");
        }

        if (bgPauseAnimator == null) {
            throw new MissingReferenceException("Colocar a referência do objeto do painel de background do pause!");
        }

        if (pauseButton == null) {
            throw new MissingReferenceException("Colocar a referência do objeto do botão de pausa!");
        }
    }

    public void VerifyCards(Card clickedCard) 
    {
        //Debug.Log("Entrou no CardManager.VerifyCards()");
        if (firstFlippedCard == null) {
            //Debug.Log("firstFlippedCard == null: true");
            firstFlippedCard = clickedCard;
            clickedCard.ExposeCard();
        } else {
            //Debug.Log("secondFlippedCard == null: true");
            
            if (clickedCard != firstFlippedCard) {
                if (secondFlippedCard == null) {
                    secondFlippedCard = clickedCard;
                    clickedCard.ExposeCard();
                    StartCoroutine(CheckCardsMatch());
                }
            } else {
                clickedCard.HideCard();
                firstFlippedCard = null;
            }
        }
    }

    IEnumerator CheckCardsMatch() 
    {
        if (firstFlippedCard.cardValue.Equals(secondFlippedCard.cardValue) && firstFlippedCard != secondFlippedCard) {
            //If they match
            firstFlippedCard.canFlip = false;
            secondFlippedCard.canFlip = false;
            MakeMatch();
        } else {
            //If don't match
            yield return new WaitForSeconds(1f);
            firstFlippedCard.HideCard();
            firstFlippedCard = null;
            secondFlippedCard.HideCard();
            secondFlippedCard = null;
        }
    }

    void MakeMatch() 
    {
        currentScore++;        

        Invoke("InflateMatchedCards", timeBeforePunchingMatchedCard);

        Invoke("ClearExposedCardsReferences", timeBeforePunchingMatchedCard + .1f);

        if (tipClickPictureAnimator != null) {
            if (currentScore == 1) {
                Invoke("ShowTipToClickPictures", timeBeforePunchingMatchedCard + .5f);
            }
        }

        if (currentScore >= targetScore) {
            isGameWon = true;
            if(cardInBigPicture != null) {
                cardInBigPicture.UnsetFromBigPicture();
            }
            Invoke("CallVictoryPanel", timeBeforePunchingMatchedCard + .5f);
        }
    }

    private void ShowTipToClickPictures() {
        tipClickPictureAnimator.gameObject.SetActive(true);
        tipClickPictureAnimator.SetFloat("speed", 1);
        tipClickPictureAnimator.Play("enter");
        Invoke("HideTipToClickPictures", 3f);
    }

    private void HideTipToClickPictures() {
        tipClickPictureAnimator.SetFloat("speed", -1);
        tipClickPictureAnimator.Play("enter");      
    }

    private void InflateMatchedCards() { 
        firstFlippedCard.InflateCard();
        secondFlippedCard.InflateCard();
    }

    private void ClearExposedCardsReferences() { 
        firstFlippedCard = null;
        secondFlippedCard = null;
    }

    void CallVictoryPanel() {
        pauseButton.SetActive(false);
        victoryPanelAnimator.gameObject.SetActive(true);
        victoryPanelAnimator.Play("entry");
    }

    public void PauseGame() {
        pauseButton.SetActive(false);
        canPlayTheGame = false;
        pausePanelAnimator.gameObject.SetActive(true);
        pausePanelAnimator.SetFloat("speed", 1f);
        pausePanelAnimator.Play("entry");

        bgPauseAnimator.gameObject.SetActive(true);
        bgPauseAnimator.SetFloat("speed", 1f);
        bgPauseAnimator.Play("fade");
    }

    public void UnpauseGame() {
        canPlayTheGame = true;
        pausePanelAnimator.SetFloat("speed", -1f);        
        pausePanelAnimator.Play("entry");
        Invoke("HidePausePanel", .3f);

        pauseButton.SetActive(true);
    }

    void HidePausePanel() {
        pausePanelAnimator.gameObject.SetActive(false);
        bgPauseAnimator.SetFloat("speed", -1f);
        bgPauseAnimator.gameObject.SetActive(false);

        bgPauseAnimator.Play("fade");
    }


}
