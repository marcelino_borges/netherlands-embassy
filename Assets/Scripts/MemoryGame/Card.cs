﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum CardState {
    hidden,
    exposed
}

public class Card : MonoBehaviour
{
    public float flippingDuration;
    public float bigPictureAnimDuration;
    public int matchVibrato;
    public CardState state = CardState.exposed;
    public GameObject description;
    public Vector3 originalScale;
    public Vector3 originalPosition;
    public string cardValue;
    public bool canFlip;

    private CardManager cardManager;
    private SpriteRenderer front;
    private SpriteRenderer back;
    private const int hiddenRotation = 0;
    private const int exposedRotation = 180;

    void OnMouseUp() {
        if (state == CardState.exposed && !canFlip) {
            //exposed and cannot flip, the card is matched, so can set in big picture
            if (cardManager.cardInBigPicture == null) {
                //if there is no card already in big picture
                if (!cardManager.isGameWon) {
                    SetInBigPicture();
                }
            } else {
                if (cardManager.cardInBigPicture == this) {
                    UnsetFromBigPicture();
                }
            }
        } else {
            if (cardManager.canPlayTheGame) {
                NotifyCardManager();
            }
        }        
    }

    void Start() {
        description.SetActive(false);
        originalScale = transform.localScale;
        originalPosition = transform.position;
        cardManager = GameObject.FindGameObjectWithTag("cardmanager").GetComponent<CardManager>();

        if(cardManager == null) {
            throw new MissingReferenceException("Necessário existir a referência do CardManager da cena nos Cards.");
        }

        if (description == null) {
            throw new MissingReferenceException("Necessário existir a referência do objeto description nos Cards.");
        }

        canFlip = true;
        state = CardState.hidden;

        if(state == CardState.hidden) {
            HideCard();
        } else {
            ExposeCard();
        }
        
    }

    public void SetInBigPicture() {
        //Sets
        cardManager.cardInBigPicture = this;
        cardManager.canPlayTheGame = false;
        gameObject.GetComponentInChildren<SpriteRenderer>().sortingOrder = 1;
        //Animations
        transform.DOMove(new Vector3(-3, 1, 0), bigPictureAnimDuration);
        transform.DOScale(originalScale + new Vector3(2, 2, 0), bigPictureAnimDuration).OnComplete(() => {
            description.SetActive(true);
        });
    }

    public void UnsetFromBigPicture() {
        Debug.Log("22222222222");
        //Animations
        transform.DOMove(originalPosition, bigPictureAnimDuration);
        transform.DOScale(originalScale, bigPictureAnimDuration).OnComplete(() => {
            gameObject.GetComponentInChildren<SpriteRenderer>().sortingOrder = 0;
        });
        //Sets
        description.SetActive(false);
        cardManager.cardInBigPicture = null;
        cardManager.canPlayTheGame = true;
        
    }

    public void ExposeCard() 
    {        
        if(canFlip) {
            state = CardState.exposed;
            transform.DORotate(new Vector3(0, hiddenRotation, 0), flippingDuration);            
        }
    }

    public void HideCard() 
    {        
        if(canFlip) {              
            state = CardState.exposed;
            transform.DORotate(new Vector3(0, exposedRotation, 0), flippingDuration);            
        }
    }

    public void InflateCard() {
        transform.DOPunchScale(new Vector3(.3f, .3f, 0), .5f, matchVibrato);
    }

    private void NotifyCardManager()
    {
        cardManager.VerifyCards(this);
    }
}
