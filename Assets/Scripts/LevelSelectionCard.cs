﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelSelectionCard : MonoBehaviour {
    private RectTransform originalTransform;
    private Vector2 originalSizeDelta;
    private bool isInflated;
    public GameObject description;

    private void Start() {
        originalTransform = GetComponent<RectTransform>();
        originalSizeDelta = originalTransform.sizeDelta;
        //print(originalSizeDelta);
        description.SetActive(false);
    }

    public void InflateCard() {        
        if (!isInflated) {
            originalTransform.DOSizeDelta(originalSizeDelta * 1.2f, .25f);            
            isInflated = true;
            description.SetActive(true);
        }
    }

    public void ShrinkCard() {        
        if (isInflated) {
            originalTransform.DOSizeDelta(originalSizeDelta, .25f);
            isInflated = false;
            description.SetActive(false);
        }
    }
}
