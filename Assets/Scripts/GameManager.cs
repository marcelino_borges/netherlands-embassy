﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    [HideInInspector]
    public static GameManager instance;

    public bool isMusicMuted;
    public bool isSfxMuted;

    private void Awake() {
        //When Game is initialized
        isMusicMuted = false;
        isSfxMuted = false;

        if (instance == null) {
            DontDestroyOnLoad(gameObject);
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }
    }

    private void Start() {
    }
}
