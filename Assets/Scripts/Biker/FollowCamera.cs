﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowCamera : MonoBehaviour {
    public float minX;
    public float xOffset;
    public Transform playerTransform;

    private void Start() {
        minX = transform.position.x;
        xOffset = minX - playerTransform.position.x;
    }

    private void LateUpdate() {
        Vector3 playerPosition = new Vector3(playerTransform.position.x, transform.position.y, transform.position.z);
        Vector3 cameraDesiredPosition;

        cameraDesiredPosition = new Vector3(playerTransform.position.x + xOffset, transform.position.y, transform.position.z);

        transform.position = cameraDesiredPosition;
    }

}
