﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Car : MonoBehaviour
{
    public float currentSpeed;
    public float maxSpeed = 1f;
    private Rigidbody2D rb;
    public bool isStopping;
    public float breakTorque;
    public bool isStarting;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        isStopping = false;
        breakTorque = 3.5f;
    }

    void FixedUpdate() {
        rb.velocity = new Vector2(currentSpeed, 0);
    }

    // Update is called once per frame
    void Update() {
        //Stop the biker smoothly
        if (isStopping) {
            if (currentSpeed > 0) {
                currentSpeed -= breakTorque * Time.deltaTime;
            } else if (currentSpeed < 0) {
                //Avoiding the behavior where the rideSpeed got < 0 (like -0.0083) and was moving backwards slowly
                currentSpeed = 0;
                isStopping = false;
            }
        }

        if (isStarting) {
            if (currentSpeed <= 0) {
                if(currentSpeed < maxSpeed) {
                    currentSpeed += 36 * Time.deltaTime;
                } else if (currentSpeed > maxSpeed) {
                    //Avoiding the behavior where the rideSpeed got < 0 (like -0.0083) and was moving backwards slowly
                    currentSpeed = maxSpeed;
                    isStarting = false;
                }
            }
        }
    }

    void OnCollisionEnter2D(Collision2D collision) {
        if(collision.gameObject.CompareTag("car")) {
            currentSpeed = 0;
        }
    }

    public void SetSpeed(float NewSpeed) {
        currentSpeed = NewSpeed;
        maxSpeed = NewSpeed;
    }

    public void Stop() {
        //print("car break torque: " + breakTorque);
        isStopping = true;
    }

    public void StartMoving() {
        //print("carro startou");
        currentSpeed = maxSpeed;
        //isStopping = false;
        //isStarting = true;
    }


}

