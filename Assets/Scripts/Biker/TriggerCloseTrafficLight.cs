﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerCloseTrafficLight : MonoBehaviour {

    [HideInInspector]
    public BikerManager bikerManager;

    private TrafficLight trafficLight;

    void Start() {
        bikerManager = GameObject.FindGameObjectWithTag("bikermanager").GetComponent<BikerManager>();
        trafficLight = GameObject.Find("TrafficLight").GetComponent<TrafficLight>();
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.tag.Equals("biker")) {
            StartCoroutine(bikerManager.trafficLight.CloseTrafficOverNormalCycle());
            StartCoroutine(bikerManager.ShowTipStopRiding());
        } else if(collision.gameObject.CompareTag("car")) {
            //print("Colidiu pra parar carro");
            if(trafficLight != null) {
                if(trafficLight.status == TrafficStatus.Closed || trafficLight.status == TrafficStatus.Attention) {
                    collision.gameObject.GetComponent<Car>().Stop();
                }
            }
        }
    }

}
