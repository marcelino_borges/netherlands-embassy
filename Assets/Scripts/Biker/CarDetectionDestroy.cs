﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarDetectionDestroy : MonoBehaviour
{
    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.gameObject.CompareTag("car")) {
            Destroy(collision.gameObject);
        }
    }
}
