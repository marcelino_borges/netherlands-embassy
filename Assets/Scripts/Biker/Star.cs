﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Star : MonoBehaviour
{
    public bool active;
    public Image child;
    public Animator animator;

    // Start is called before the first frame update
    void Start() {
        active = false;
        child.enabled = false;
    }

    public void Activate() {
        active = true;
        child.enabled = true;
        Animate();
    }

    public void Animate() {
        animator.Play("StarEarn");
    }

    
}
