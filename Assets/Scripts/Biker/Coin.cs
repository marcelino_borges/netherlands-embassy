﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

    public GameObject coinExplosion;

    public int value = 1;

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag.Equals("biker")) {
            collision.gameObject.GetComponent<Biker>().CollectCoin(value);

            GameObject explosion = Instantiate(coinExplosion, transform.position, transform.rotation);

            Destroy(explosion, 0.15f);
            Destroy(gameObject);

        }
    }
}
