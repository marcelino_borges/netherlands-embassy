﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxScrolling : MonoBehaviour {

    public float backgroundSize;
    public Biker biker;
    public float parallaxSpeed;
    public bool doScroll, doParallax;

    private Transform cameraTransform;
    private Transform[] layers;
    private float viewZone = 10;
    private int leftIndex;
    private int rightIndex;
    private float lastCameraX;

    private void Start() {
        biker = GameObject.FindGameObjectWithTag("biker").GetComponent<Biker>();        

        cameraTransform = Camera.main.transform;
        lastCameraX = cameraTransform.position.x;
        layers = new Transform[transform.childCount];

        for(int i = 0; i < transform.childCount; i++) {
            layers[i] = transform.GetChild(i);
        }        

        leftIndex = 0;
        rightIndex = layers.Length - 1;
    }

    private void Update() {
        if (doParallax) {
            float deltaX = cameraTransform.position.x - lastCameraX;
            transform.position += Vector3.right * (deltaX * parallaxSpeed);
        }

        lastCameraX = cameraTransform.position.x;

        if (doScroll) {
            if (cameraTransform.position.x < (layers[leftIndex].transform.position.x + viewZone)) {
                ScrollLeft();
            }
            if (cameraTransform.position.x > (layers[rightIndex].transform.position.x - viewZone)) {
                ScrollRight();
            }
        }
    }

    private void ScrollLeft() {
        int lastRight = rightIndex;
        layers[rightIndex].position = new Vector3((layers[leftIndex].position.x - backgroundSize), layers[rightIndex].position.y);
        //layers[rightIndex].position = Vector3.right * (layers[leftIndex].position.x - backgroundSize);
        leftIndex = rightIndex;
        rightIndex--;

        if(rightIndex < 0) {
            rightIndex = layers.Length - 1;
        }
    }

    private void ScrollRight() {
        int lastLeft = leftIndex;
        layers[leftIndex].position = new Vector3((layers[rightIndex].position.x + backgroundSize), layers[rightIndex].position.y);
        rightIndex = leftIndex;
        leftIndex++;

        if (leftIndex == layers.Length) {
            leftIndex = 0;
        }

    }

}
