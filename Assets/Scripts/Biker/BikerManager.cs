﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BikerManager : MonoBehaviour {

    public Biker biker;
    public AudioClip song_part1;
    public AudioClip song_part2;
    public MusicManager musicManager;

    public float faultCounter;
    public float maxFaultCounter = 100;

    //public float currentProgress;

    public Slider progressBarUI;
    public Slider faultBarUI;

    public Transform finalBikerPosition;
    public float currentBikerPosition;
    public float currentBikerPositionPercentage;

    public bool isGameWon = false;
    public bool isGameOver= false;
    public bool hasShownTipStopRiding = false;
    public bool canPlayTheGame = true;

    public Animator victoryPanelAnimatorUI;
    public Animator gameOverPanelAnimatorUI;
    public Animator pausePanelAnimator, bgPauseAnimator, missionInfoAnimator;

    public TrafficLight trafficLight;
    public Text gameOverTextUI;

    public GameObject tipStartRidingUI;
    public GameObject tipStopRidingUI;
    public GameObject pauseButton;

    public int coinsInLevel;
    public int coinsCollected = 0;
    public Text coinsCounter_UI;
    public Star star1_UI;
    public Star star2_UI;
    public Star star3_UI;
    public int starsEarned;

    void Start() { 
        VerifyMandatoryActors();

        musicManager = GameObject.FindGameObjectWithTag("musicManager").GetComponent<MusicManager>();
        SetMusic();

        currentBikerPosition = biker.transform.position.x;

        faultCounter = maxFaultCounter;

        StartCoroutine(ShowTipStartRiding());

        ShowMissionInfo();

        coinsInLevel = GameObject.FindObjectsOfType<Coin>().Length;

        starsEarned = 0;

    }

    private void SetMusic() {
        song_part1 = Resources.Load<AudioClip>("Sounds/Songs/bike1");
        song_part2 = Resources.Load<AudioClip>("Sounds/Songs/bike2");

        if (musicManager != null) {
            musicManager.SetSongs(new AudioClip[] { song_part1, song_part2 });
        }
    }

    void Update() {
        //progressBar.value = currentProgress;
        faultBarUI.value = faultCounter;

        currentBikerPosition = biker.transform.position.x;
        currentBikerPositionPercentage = (currentBikerPosition * 100) / biker.temp_finalPositionOnX /*finalBikerPosition.position.x*/;
        progressBarUI.value = currentBikerPositionPercentage;
    }

    public IEnumerator ShowTipStartRiding() {
        yield return new WaitForSeconds(3f);

        while(biker.currentRideSpeed <= 0) {
            tipStartRidingUI.SetActive(true);
            yield return new WaitForSeconds(1);
            tipStartRidingUI.SetActive(false);
            yield return new WaitForSeconds(.5f);
        }
    }

    public IEnumerator ShowTipStopRiding() {
        if(!hasShownTipStopRiding) {
            hasShownTipStopRiding = true;
            while(biker.currentRideSpeed > 0) {
                tipStopRidingUI.SetActive(true);
                yield return new WaitForSeconds(1);
                tipStopRidingUI.SetActive(false);
                yield return new WaitForSeconds(.5f);
            }
        }
    }

    private void VerifyMandatoryActors() {
        if (progressBarUI == null) {
            throw new MissingReferenceException("Colocar a referência do slider do progresso!");
        }

        if (faultBarUI == null) {
            throw new MissingReferenceException("Colocar a referência do slider das faltas!");
        }

        if (victoryPanelAnimatorUI == null) {
            Debug.LogWarning("Esqueceu de setar o objeto do painel de vitória pelo inspector");
        }

        if (gameOverPanelAnimatorUI == null) {
            Debug.LogWarning("Esqueceu de setar o objeto do painel de game over pelo inspector");
        }

        if (trafficLight == null) {
            Debug.LogWarning("Esqueceu de setar o objeto trafficLight pelo inspector.");
        }

        if (gameOverTextUI == null) {
            Debug.LogWarning("Esqueceu de setar o objeto gameOverTextUI pelo inspector.");
        }

        if (tipStartRidingUI == null) {
            Debug.LogWarning("Esqueceu de setar o objeto da UI tipStartRiding pelo inspector.");
        }

        if (tipStopRidingUI == null) {
            Debug.LogWarning("Esqueceu de setar o objeto da UI tipStopRiding pelo inspector.");
        }
    }

    public void LoseGameByLightsTrespass() {
        isGameOver = true;
        biker.StopRiding();
        SetGameOverMsg("Aqui na Holanda somos obrigados a parar antes da faixa de pedestre quando o sinal de trânsito fechar. Mesmo na ciclovia.");
        ShowGameOverPanel();
    }

    public void LoseGameByFaults() {
        isGameOver = true;
        biker.StopRiding();
        SetGameOverMsg("Aqui na Holanda não podemos sair da ciclovia durante nosso trajeto...");
        ShowGameOverPanel();
    }

    public void LoseGameByCarAccident() {
        isGameOver = true;
        biker.StopRiding();
        SetGameOverMsg("Você saiu da ciclovia e foi atingido por um carro. Mais cuidado da próxima vez...");
        ShowGameOverPanel();
    }

    public int CalculateStarsEarned() {
        if (coinsCollected >= coinsInLevel) {
            //If got all coins
            if (faultCounter >= (maxFaultCounter / 2)) {
                //If faults aren't less than 50%
                return 3;
            } else {
                //If faults are less than 50%
                return 2;
            }
        } else if (coinsCollected >= (coinsInLevel / 2)) {
            //If didn't get all coins, but got more than 50% of coins
            if (faultCounter >= (maxFaultCounter / 2)) {
                //If faults aren't less than 50%
                return 2;
            } else {
                //If faults are less than 50%
                return 1;
            }
        } else {
            //If got less than 50% of coins
            if (faultCounter >= (maxFaultCounter / 2)) {
                //If faults aren't less than 50%
                return 1;
            } else {
                //If faults are less than 50%
                return 0;
            }
        }
    }

    public void WinGame() {
        isGameWon = true;
        ShowVictoryPanel();
        StartCoroutine(ShowStarsAnimatedCoRoutine());

    }

    public IEnumerator ShowStarsAnimatedCoRoutine() {
        yield return new WaitForSeconds(.3f); //Wait the victory panel finish the entry animation
        int starsEarned = CalculateStarsEarned();

        switch(starsEarned) {
            case 3:
            star1_UI.Activate();
            yield return new WaitForSeconds(.4f);
            SoundManager.PlaySound("star");
            yield return new WaitForSeconds(.4f);
            star2_UI.Activate();
            yield return new WaitForSeconds(.4f);
            SoundManager.PlaySound("star");
            yield return new WaitForSeconds(.4f);
            star3_UI.Activate();
            yield return new WaitForSeconds(.4f);
            SoundManager.PlaySound("star");
            break;

            case 2:
            star1_UI.Activate();
            yield return new WaitForSeconds(.4f);
            SoundManager.PlaySound("star");
            yield return new WaitForSeconds(.4f);
            star2_UI.Activate();
            yield return new WaitForSeconds(.4f);
            SoundManager.PlaySound("star");
            break;

            case 1:
            star1_UI.Activate();
            yield return new WaitForSeconds(.4f);
            SoundManager.PlaySound("star");
            break;
        }

    }

    public void ShowVictoryPanel() {
        victoryPanelAnimatorUI.gameObject.SetActive(true);
        victoryPanelAnimatorUI.Play("entry");
    }

    public void ShowGameOverPanel() {
        gameOverPanelAnimatorUI.gameObject.SetActive(true);
        gameOverPanelAnimatorUI.Play("entry");
    }

    private void SetGameOverMsg(string msg) {
        gameOverTextUI.text = msg;
    }

    public void PauseGame() {
        biker.StopRiding();
        pauseButton.SetActive(false);
        canPlayTheGame = false;
        ShowPauseBgPanel();
        pausePanelAnimator.gameObject.SetActive(true);
        pausePanelAnimator.SetFloat("speed", 1f);
        pausePanelAnimator.Play("entry");
    }

    public void UnpauseGame() {
        biker.StartRiding();
        canPlayTheGame = true;
        pausePanelAnimator.SetFloat("speed", -1f);
        pausePanelAnimator.Play("entry");
        Invoke("HideBgPausePanel", .3f);
        pauseButton.SetActive(true);
    }

    void ShowPauseBgPanel() { 
        bgPauseAnimator.gameObject.SetActive(true);
        bgPauseAnimator.SetFloat("speed", 1f);
        bgPauseAnimator.Play("fade");
    }

    void HideBgPausePanel() {
        pausePanelAnimator.gameObject.SetActive(false);
        bgPauseAnimator.SetFloat("speed", -1f);
        bgPauseAnimator.Play("fade");
        StartCoroutine(DeactivateBg());
    }

    IEnumerator DeactivateBg() {
        yield return new WaitForSeconds(1f);
        bgPauseAnimator.gameObject.SetActive(false);
    }

    void ShowMissionInfo() {
        biker.StopRiding();
        canPlayTheGame = false;
        ShowPauseBgPanel();
        missionInfoAnimator.gameObject.SetActive(true);
        missionInfoAnimator.SetFloat("speed", 1f);
        missionInfoAnimator.Play("entry");
    }

    public void HideMissionInfo() {
        biker.StartRiding();
        canPlayTheGame = true;
        Invoke("HideBgPausePanel", .3f);
        missionInfoAnimator.SetFloat("speed", -1f);
        missionInfoAnimator.Play("entry");        
        StartCoroutine(DeactivateMissionInfoPanel());
    }

    IEnumerator DeactivateMissionInfoPanel() {
        yield return new WaitForSeconds(1f);
        missionInfoAnimator.gameObject.SetActive(false);
    }

    public void SetCoinsCounter() {
        if(coinsCollected > 9) {
            coinsCounter_UI.text = coinsCollected.ToString();
        } else {
            coinsCounter_UI.text = "0" + coinsCollected.ToString();
        }
    }
}

