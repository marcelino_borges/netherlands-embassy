﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TrafficStatus {
    Open,
    Closed,
    Attention
}

public class TrafficLight : MonoBehaviour
{
    public TrafficStatus status;

    public SpriteRenderer redLight;
    public SpriteRenderer yellowLight;
    public SpriteRenderer greenLight;

    //Timings in seconds
    public float closedTime = 7;
    public float openTime = 15;
    public float attentionTime = 1;

    [HideInInspector]
    public BikerManager bikerManager;

    public bool wasTrespassed = false;

    void Start() {
        bikerManager = GameObject.FindGameObjectWithTag("bikermanager").GetComponent<BikerManager>();

        if (redLight == null) {
            Debug.LogWarning("Esqueceu de setar o objeto redLight do sinal pelo inspector.");
        }

        if (yellowLight == null) {
            Debug.LogWarning("Esqueceu de setar o objeto yellowLight do sinal pelo inspector.");
        }

        if (greenLight == null) {
            Debug.LogWarning("Esqueceu de setar o objeto greenLight do sinal pelo inspector.");
        }

        //OpenTraffic();
        StartCoroutine(StartTrafficCycle());
    }

    /// <summary>
    /// Starts the routine from the CLOSED/RED status
    /// </summary>
    public IEnumerator StartTrafficCycle() {
        //StartCoroutine(CloseTraffic());
        OpenTraffic();
        yield return new WaitForSeconds(openTime);
        CallAttentionToClose();
        yield return new WaitForSeconds(attentionTime);
        CloseTraffic();
        yield return new WaitForSeconds(closedTime);
        //CallAttentionToClose();
        //yield return new WaitForSeconds(attentionTime);
        StartCoroutine(StartTrafficCycle());
        
    }

    public IEnumerator CloseTrafficOverNormalCycle() {
        StopCoroutine(StartTrafficCycle());
        CloseTraffic();
        yield return new WaitForSeconds(closedTime);
        StartCoroutine(StartTrafficCycle());
    }

    public void CloseTraffic() {        
        TurnLightOn("red");
        status = TrafficStatus.Closed;
    }

    public void OpenTraffic() {
        TurnLightOn("green");
        status = TrafficStatus.Open;

        bikerManager.biker.blinkWarningToContinue = true;

        GameObject[] carsInLevel = GameObject.FindGameObjectsWithTag("car");

        foreach(GameObject carObj in carsInLevel) {
            carObj.GetComponent<Car>().StartMoving();
            print("aaa");
        }
    }

    public void CallAttentionToClose() {
        TurnLightOn("yellow");
        status = TrafficStatus.Attention;
    }

    private void TurnLightOn(string lightName) {
        switch(lightName) {
            case "red":
                redLight.enabled = true;
                yellowLight.enabled = false;
                greenLight.enabled = false;
            break;
            case "yellow":
                redLight.enabled = false;
                yellowLight.enabled = true;
                greenLight.enabled = false;
            break;
            case "green":
                redLight.enabled = false;
                yellowLight.enabled = false;
                greenLight.enabled = true;

            break;
        }
    }
}
