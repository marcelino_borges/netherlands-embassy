﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Biker : MonoBehaviour {

    //public bool canRide;
    public bool isDead;

    public float pushUpForce;
    public const float maxPushUpForce = 3.2f;
    public const float normalPushUpForce = 0;

    public float fallSpeed;
    public readonly float normalFallSpeed = 0f;
    public readonly float maxFallSpeed = -3.2f;

    private float smoothMovement;

    private bool isStopping = false;
    public float currentRideSpeed;
    public float normalRideSpeed = 8;

    public const int ridingDirection = -1; //Valor anterior: -1
    private const int stoppedSpeedForAnimation = 5;
    public readonly int temp_finalPositionOnX = 100;
    public readonly float maxY  = 2.7f;
    public readonly float minY = -2.07f;
    public readonly float faultyY = 1.58f;
    private bool isPushingUp;
    public bool fadeWarningFault = false;
    public bool blinkWarningToContinue = false;

    [HideInInspector]
    public BikerManager bikerManager;
    private Rigidbody2D rb;

    public GameObject faultWarning;
    public GameObject continueWarning;
    public Animator animator;

    private AudioSource audioSource;
    public float breakTorque;
    public float startingTorque;
    private bool isStartingRiding;
    private bool isPressingPushUpButton;
    private bool fadeWarningContinue = false;
    private bool isPushingDown;

    void Awake() {
        animator = GetComponent<Animator>();
        bikerManager = GameObject.FindGameObjectWithTag("bikermanager").GetComponent<BikerManager>();
        audioSource = GetComponent<AudioSource>();
        rb = GetComponent<Rigidbody2D>();
    }

    void Start() {
        isPressingPushUpButton = false;
        isPushingUp = false;
        blinkWarningToContinue = false;

        VerifyMandatoryActors();

        audioSource.Stop();
        currentRideSpeed = 0;
        fallSpeed = normalFallSpeed;

        breakTorque = 8f;
        smoothMovement = 12f;
    }    

    void FixedUpdate() {
        //Faults counter
        if (transform.position.y < minY || (transform.position.y >= minY && transform.position.y < faultyY)) {
            if(!fadeWarningFault) {
                faultWarning.GetComponent<SpriteRenderer>().DOFade(0f, .5f).SetDelay(1).OnComplete(() => fadeWarningFault = true);
            } else {
                faultWarning.GetComponent<SpriteRenderer>().DOFade(1, .5f).SetDelay(1).OnComplete(() => fadeWarningFault = false);
            }

            if (bikerManager.faultCounter > 0) {
                //Moment when the biker is on the highway
                bikerManager.faultCounter -= 0.1f;                
            } else {
                if (!bikerManager.isGameOver) {
                    bikerManager.LoseGameByFaults();
                }
            }
        } else {
            faultWarning.GetComponent<SpriteRenderer>().DOFade(0f, 1f).SetDelay(1).OnComplete(() => fadeWarningFault = false);
        }

        //MIN Y BOUNDARIES
        if (transform.position.y <= minY) {
            fallSpeed = normalFallSpeed;
        }

        //WHEN REACHING FINAL POSITION IN PATH
        if (transform.position.x > /*temp_finalPositionOnX*/ bikerManager.finalBikerPosition.position.x) {
            StopRiding();
            
            if(!bikerManager.isGameWon && !bikerManager.isGameOver) {
                bikerManager.WinGame();
            }
        }

        // VELOCITY
        rb.velocity = new Vector2(
            transform.localScale.x * currentRideSpeed * ridingDirection, 
            0 + fallSpeed + pushUpForce
            );


    }

    private void Update() {

        audioSource.mute = SoundManager.instance.isMuted;

        if (blinkWarningToContinue) {
            if (!fadeWarningContinue) {
                continueWarning.GetComponent<SpriteRenderer>().DOFade(0f, .5f).SetDelay(1).OnComplete(() => fadeWarningContinue = true);
            } else {
                continueWarning.GetComponent<SpriteRenderer>().DOFade(1, .5f).SetDelay(1).OnComplete(() => fadeWarningContinue = false);
            }

            if(currentRideSpeed > 0) {
                blinkWarningToContinue = false;
            }
        } else {
            continueWarning.GetComponent<SpriteRenderer>().DOFade(0f, 1f).SetDelay(1).OnComplete(() => fadeWarningContinue = false);
        }

        //Stop the biker smoothly
        if(isStopping) {
            if (currentRideSpeed > 0) {
                currentRideSpeed -= breakTorque * Time.deltaTime;
            }  else if (currentRideSpeed < 0) {
                //Avoiding the behavior where the rideSpeed got < 0 (like -0.0083) and was moving backwards slowly
                currentRideSpeed = 0;
                isStopping = false;
                audioSource.Stop();
            }
        }

        //PUSH UP
        if (isPushingUp) {
            if (transform.position.y < maxY) {
                if (pushUpForce < maxPushUpForce) {
                    pushUpForce += smoothMovement * Time.deltaTime;
                } else {
                    pushUpForce = maxPushUpForce;
                }
            } else {
                pushUpForce = normalPushUpForce;
            }

            //if (transform.position.y <= minY) {
            //    fallSpeed = 0;
            //}
        } else {
            pushUpForce = normalPushUpForce;
        }

        //PUSH DOWN
        if (isPushingDown) {
            if (transform.position.y > minY) {
                if (fallSpeed > maxFallSpeed) {
                    fallSpeed -= smoothMovement * Time.deltaTime;
                } else {
                    fallSpeed = maxFallSpeed;
                }
            } else {
                fallSpeed = normalFallSpeed;
            }

            /*if (transform.position.y <= minY) {
                fallSpeed = 0;
            }*/
        } else {
            fallSpeed = normalFallSpeed;
        }

        //Starts riding smoothly
        if (isStartingRiding) {
            if (currentRideSpeed < normalRideSpeed) {
                currentRideSpeed += startingTorque * Time.deltaTime;
            } else {
                //if rideSpeed overflows the normalRideSpeed
                //set rideSpeed to the exact value of normalRideSpeed
                currentRideSpeed = normalRideSpeed;
                isStartingRiding = false;
            }
        }

        //animator.SetBool("Moving", rideSpeed > 0);
        //animator.SetFloat("speed", ConvertRange(rideSpeed, 0, normalRideSpeed, 0, 1));
        if(currentRideSpeed > 0 && !isStopping) {
            animator.SetBool("Moving", true);
        }

        if (Input.GetKeyDown(KeyCode.RightArrow)) {
            StartRiding();
        } 

        if (Input.GetKeyDown(KeyCode.LeftArrow)) {
            StopRiding();
        }

        if (Input.GetKeyDown(KeyCode.UpArrow)) {            
            PushUp();
        } else if (Input.GetKeyUp(KeyCode.UpArrow)) {
            DontPushUp();
            /*if(!isPressingPushUpButton) {
                DontPushUp();
            }*/
        }

        if(Input.GetKeyDown(KeyCode.DownArrow)) {
            PushDown();
        } else if(Input.GetKeyUp(KeyCode.DownArrow)) {
            DontPushDown();
        }
    }

    void OnTriggerEnter2D(Collider2D collision) {
        if(!isDead) {
            if (collision.gameObject.CompareTag("car")) {
                bikerManager.LoseGameByCarAccident();
                collision.gameObject.GetComponent<Car>().Stop();
                SoundManager.PlaySound("carAccident");
                isDead = true;
            }
        }
    }

    private float ConvertRange(float value, float inputFrom, float inputTo, float outputFrom, float outputTo) {
        return (value - inputFrom) / (inputTo - inputFrom) * (outputTo - outputFrom) + outputFrom;
    }

    public void StartRiding() {
        if (bikerManager.canPlayTheGame) {
            if(currentRideSpeed < normalRideSpeed) {
                //rideSpeed = normalRideSpeed;
                //fallSpeed = normalFallSpeed;
                if(currentRideSpeed <= 0) {
                    audioSource.Play();
                }
                isStopping = false;
                isStartingRiding = true;
            }
        }
    }

    public void StopRiding() {
        if (bikerManager.canPlayTheGame) {
            if(currentRideSpeed > 0) {
                isStopping = true;
                fallSpeed = 0;
                animator.SetBool("Moving", false);
            }
        }
    }

    public void PressedPushUpButton() {
        isPressingPushUpButton = true;
        PushUp();
    }

    public void ReleasedPushUpButton() {
        isPressingPushUpButton = false;
        DontPushUp();
    }

    public void PushUp() {
        if (bikerManager.canPlayTheGame) {
            if (currentRideSpeed > 0) {
                isPushingUp = true;
            }
        }
    }

    public void PushDown() {
        if (bikerManager.canPlayTheGame) {
            if (currentRideSpeed > 0) {
                isPushingDown = true;
            }
        }
    }

    public void DontPushDown() {
        if (bikerManager.canPlayTheGame) {
            if (currentRideSpeed > 0) {
                isPushingDown = false;
            }
        }
    }

    public void DontPushUp() {
        if (bikerManager.canPlayTheGame) {
            isPushingUp = false;
        }
    }

    private void VerifyMandatoryActors() {
        /*if (pushUpForce == 0) {
            pushUpForce = normalPushUpForce;
            Debug.LogError("Esqueceu de setar o pushUpForce do biker pelo inspector. Set automático para " + maxPushUpForce);
        }*/

        if (faultWarning == null) {
            Debug.LogError("Esqueceu de setar o objeto biker_warning pelo inspector");
        }
    }

    public void CollectCoin(int value) {
        bikerManager.coinsCollected += value;
        bikerManager.SetCoinsCounter();
        SoundManager.PlaySound("coin_pickup");
    }
}
