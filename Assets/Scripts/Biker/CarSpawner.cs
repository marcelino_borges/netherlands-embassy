﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpawner : MonoBehaviour
{
    public GameObject[] carTypes;
    public float secondsBetweenSpawn;
    public float minTime;
    public float maxTime;
    private float elapsedTime = 0.0f;

    // Start is called before the first frame update
    void Start() {
        if(carTypes == null || carTypes.Length <= 0) {
            Debug.LogError("Falta setar os tipos de carro a serem spawnados no carTypes do CarSpawner");
        }
        secondsBetweenSpawn = SortTimeToNextSpawn();
    }

    float SortTimeToNextSpawn() {
        return Random.Range(minTime, maxTime);
    }

    // Update is called once per frame
    void Update() {
        elapsedTime += Time.deltaTime;

        if (elapsedTime > secondsBetweenSpawn) {
            elapsedTime = 0;
            int sortedCarIndex = Random.Range(0, carTypes.Length - 1);
            GameObject newCar = Instantiate(carTypes[sortedCarIndex], transform.position, transform.rotation) as GameObject;
            if(newCar != null) {
                newCar.GetComponent<Car>().SetSpeed(5f);
            }
            secondsBetweenSpawn = SortTimeToNextSpawn();

            //print("euuu");
        }
    }
}

