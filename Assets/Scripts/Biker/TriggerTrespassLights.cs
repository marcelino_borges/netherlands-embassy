﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerTrespassLights : MonoBehaviour {

    [HideInInspector]
    public BikerManager bikerManager;

    void Start() {
        bikerManager = GameObject.FindGameObjectWithTag("bikermanager").GetComponent<BikerManager>();
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag.Equals("biker")) {
            CheckLightTrespass();
        }
    }

    private void CheckLightTrespass() {
        if (bikerManager.trafficLight.status == TrafficStatus.Closed) {
            bikerManager.trafficLight.wasTrespassed = true;
            bikerManager.LoseGameByLightsTrespass();
        }
    }
}
