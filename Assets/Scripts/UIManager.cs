﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour {

    public AudioClip song_part1;
    public AudioClip song_part2;
    public MusicManager musicManager;
    [HideInInspector]
    public static UIManager instance;
    public GameManager gameManager;

    private void Awake() {

        if(SceneManager.GetActiveScene().buildIndex == 0 || SceneManager.GetActiveScene().buildIndex == 1) {
            if (instance == null) {
                DontDestroyOnLoad(gameObject);
                instance = this;
            } else {
                Destroy(gameObject);
            }
        } 
        musicManager = GameObject.FindGameObjectWithTag("musicManager").GetComponent<MusicManager>();
        gameManager = GameObject.FindGameObjectWithTag("gameManager").GetComponent<GameManager>();
        if (gameManager != null) {
            musicManager.SetIsMuted(gameManager.isMusicMuted);

        }


    }

    private void OnLevelWasLoaded(int level) {
        if (level != 0 && level != 1) {
            Destroy(gameObject);
        }

       
    }

    // Start is called before the first frame update
    void Start() {
        print("Start() do UIManager");
        SetMusic();
    }

    private void SetMusic() {
        song_part1 = Resources.Load<AudioClip>("Sounds/Songs/menu1");
        song_part2 = Resources.Load<AudioClip>("Sounds/Songs/menu2");

        if (musicManager != null) {
            musicManager.SetSongs(new AudioClip[] { song_part1, song_part2 });
        }
    }
}
