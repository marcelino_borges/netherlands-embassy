﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour
{
    [HideInInspector]
    public static MusicManager instance;
    public GameManager gameManager;  
    public AudioSource[] audioSources;
    private int currentSongIndex;

    public bool isMuted;    

    private bool isTheFirstTimePlaying;

    private void Awake() {
        if (instance == null) {
            DontDestroyOnLoad(gameObject);
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }
    }

    void Start() {
        currentSongIndex = 0;
        isMuted = false;
        isTheFirstTimePlaying = true;
        //print("Start do MusicManager");
        /*if (gameManager != null) 
            if(!GameManager.isMusicMuted)
                StartCoroutine(PlaySongsCoroutine());   */
    }

    public void SetSongs(AudioClip[] songs) {
        
        if (songs != null && songs.Length > 0) {            
            for(int i = 0; i < audioSources.Length; i++) {
                if(songs[i] != null) {
                    audioSources[i].clip = songs[i];
                }
            }
        }
        if(!isMuted) {
            //print("isMuted = " + isMuted);
            StartCoroutine(PlaySongsCoroutine());
        }
    }

    private IEnumerator PlaySongsCoroutine() {
        float songDuration;

        if(isTheFirstTimePlaying) {
             songDuration = audioSources[currentSongIndex].clip.length/* - 0.12f*/;
            isTheFirstTimePlaying = false;
        } else {
             songDuration = audioSources[currentSongIndex].clip.length;
        }

        audioSources[currentSongIndex].Play();   
        yield return new WaitForSeconds(songDuration);           
        if(currentSongIndex == (audioSources.Length - 1)) {
            currentSongIndex = 0;
        } else {
            currentSongIndex++;
        }
        StartCoroutine(PlaySongsCoroutine());
    }

    public void invertMute() {
        bool newValue = true;

        if (instance.isMuted) {
            newValue = false;
        }

        SetIsMuted(newValue);
    }

    public void SetIsMuted(bool mute) {
        isMuted = mute;
        gameManager = GameObject.FindGameObjectWithTag("gameManager").GetComponent<GameManager>();
        if(gameManager != null) {
            gameManager.isMusicMuted = mute;
        }
        foreach (AudioSource audioSource in audioSources) {
            if(audioSource != null) {
                audioSource.mute = mute;
            }
        }
    }


}
