﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ConfigButton : MonoBehaviour {
    public bool isShowingPanel = false;
    //Panel behind the the config config to close the configs panel clicking outside
    public Button closeConfigsPanelButton; 
    public Animator canvas;

    public void ShowConfigsPanel() {
        SoundManager.PlaySound("button_back");        

        if (SceneManager.GetActiveScene().buildIndex == 0) {

            if (!isShowingPanel) {
                canvas.Play("ShowConfigsPanel");
                isShowingPanel = true;

                if (closeConfigsPanelButton != null)
                    closeConfigsPanelButton.gameObject.SetActive(true);

            } else {
                canvas.Play("HideConfigsPanel");

                if (closeConfigsPanelButton != null)
                    closeConfigsPanelButton.gameObject.SetActive(false);

                isShowingPanel = false;
            }
        } else if (SceneManager.GetActiveScene().buildIndex == 1) {
            if (!isShowingPanel) {
                canvas.Play("Configs_Panel_LevelSelection");

                if (closeConfigsPanelButton != null)
                    closeConfigsPanelButton.gameObject.SetActive(true);

                isShowingPanel = true;
            } else {
                canvas.Play("Configs_Panel_LevelSelection_Hide");

                if (closeConfigsPanelButton != null)
                    closeConfigsPanelButton.gameObject.SetActive(false);

                isShowingPanel = false;
            }
        }
    }
}

