﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConfigsPanel : MonoBehaviour {

    public MusicManager musicManager;
    public SoundManager soundManager;

    private void Start() {
        
        soundManager = GameObject.FindObjectOfType<SoundManager>();
    }

    public void MuteMusic() {
        musicManager = GameObject.FindObjectOfType<MusicManager>();
        if(musicManager != null) {
            musicManager.invertMute();
        }
    }
    
    public void MuteSfx() {
        soundManager.Mute();
    }
}
