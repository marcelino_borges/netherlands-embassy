﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelSelection : MonoBehaviour {

    public Animator canvasAnimator;

    private void Start() {
        canvasAnimator = GetComponent<Animator>();
    }

    public void LoadMainMenu() {
        MakeWooshSound();
        SceneManager.LoadScene(0);
    }

    public void GoBackToMainMenu() {
        MakeClickSound();
        SceneManager.LoadScene(0);
    }

    public void LoadLevelSelection() {
        MakeWooshSound();
        SceneManager.LoadScene(1);
    }

    public void LoadGame1() {
        MakeWooshSound();
        canvasAnimator.Play("LevelSelectionExit");
        Invoke("LoadGame1Delayed", 0.5f);
    }

    public void LoadGame1Delayed() {
        /*if (SceneManager.GetActiveScene().buildIndex == 1) {
            GameObject musicManagerObject = GameObject.Find("MusicManager");
            MusicManager musicManager = musicManagerObject.GetComponent<MusicManager>();

            try {
                MusicManager.DestroyItself();
            } catch {
                Debug.Log("Erro ao tentar destruir o MusicManager no level de buildIndex 1");
            }
        }*/
        SceneManager.LoadScene(2);
    }

    public void LoadGame2() {
        MakeWooshSound();
        canvasAnimator.Play("LevelSelectionExit");
        Invoke("LoadGame2Delayed", 0.5f);
    }

    public void LoadGame2Delayed() {
        SceneManager.LoadScene(3);        
        /*if (SceneManager.GetActiveScene().buildIndex == 1) {
            GameObject musicManagerObject = GameObject.Find("MusicManager");
            MusicManager musicManager = musicManagerObject.GetComponent<MusicManager>();

            try {
                MusicManager.DestroyItself();
            } catch {
                Debug.Log("Erro ao tentar destruir o MusicManager no level de buildIndex 1");
            }
        }*/
    }

    public void LoadGame3() {
        MakeWooshSound();
        canvasAnimator.Play("LevelSelectionExit");
        Invoke("LoadGame3Delayed", 0.5f);
    }

    public void LoadGame3Delayed() {
        SceneManager.LoadScene(4);
        /*if (SceneManager.GetActiveScene().buildIndex == 1) {
            GameObject musicManagerObject = GameObject.Find("MusicManager");
            MusicManager musicManager = musicManagerObject.GetComponent<MusicManager>();

            try {
                MusicManager.DestroyItself();
            } catch {
                Debug.Log("Erro ao tentar destruir o MusicManager no level de buildIndex 1");
            }
        }*/
    }

    public void RestartScene() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void MakeWooshSound() {
        try {
            SoundManager.PlaySound("woosh_exit_menu");
        } catch {
            Debug.LogError("Put the SoundManager prefab in the scene!");
        }
    }

    public void MakeClickSound() { 
        try {
            SoundManager.PlaySound("button_back");
        } catch {
            Debug.LogError("Put the SoundManager prefab in the scene!");
        }
    }
}
