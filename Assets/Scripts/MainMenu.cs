﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public Animator animator;

    private void Start() {
        animator = gameObject.GetComponent<Animator>();
    }

    public void HideObjects() {
        animator.Play("MainMenuExit");
        Invoke("LoadLevelSelection", 0.5f);
    }

    public void LoadLevelSelection() {
        SceneManager.LoadScene(1);
    }
}
