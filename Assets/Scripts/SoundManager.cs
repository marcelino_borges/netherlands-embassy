﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    [HideInInspector]
    public static SoundManager instance;
    public static AudioClip waterFlow, victory1, button_back, woosh_exit_menu, coin_pickup, star, carAccident;
    public static AudioSource audioSource;

    public bool isMuted;

    private void Awake() {
        if (instance == null) {
            DontDestroyOnLoad(gameObject);
            instance = this;
        } else if (instance != this) {
            Destroy(gameObject);
        }
    }

    // Start is called before the first frame update
    void Start() {
        audioSource = GetComponent<AudioSource>();

        waterFlow = Resources.Load<AudioClip>("Sounds/Sfx/Pipes/water_flow");
        victory1 = Resources.Load<AudioClip>("Sounds/Sfx/victory1");
        button_back = Resources.Load<AudioClip>("Sounds/Sfx/UI/button_back");
        woosh_exit_menu = Resources.Load<AudioClip>("Sounds/Sfx/UI/woosh_exit_menu");
        coin_pickup = Resources.Load<AudioClip>("Sounds/Sfx/Biker/coin_pickup");
        star = Resources.Load<AudioClip>("Sounds/Sfx/Biker/star");
        carAccident = Resources.Load<AudioClip>("Sounds/Sfx/Biker/carAccident");
    }

    public static void PlaySound(string clip) {
        switch (clip) {
            case "waterFlow":
                audioSource.PlayOneShot(waterFlow);
                break;
            case "victory1":
                audioSource.PlayOneShot(victory1);
                break;
            case "button_back":
                audioSource.PlayOneShot(button_back);
                break;
            case "woosh_exit_menu":
                audioSource.PlayOneShot(woosh_exit_menu);
                break;
            case "coin_pickup":
                audioSource.PlayOneShot(coin_pickup);
                break;
            case "star":
                audioSource.PlayOneShot(star);
                break;
            case "carAccident":
                audioSource.PlayOneShot(carAccident);
                break;
        }
    }

    public void Mute() {
        if(audioSource.mute) {
            audioSource.mute = false;
            instance.isMuted = false;
        } else {
            audioSource.mute = true;
            instance.isMuted = true;
        }
    }
}
