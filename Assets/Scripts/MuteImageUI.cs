﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MuteImageUI : MonoBehaviour {

    public Image muteImage;

    void Update() {
        if (gameObject.name.Equals("SfxButton")) {
            if (SoundManager.instance.isMuted) {
                if (!muteImage.enabled) {
                    muteImage.enabled = true;
                }
            } else {
                if (muteImage.enabled) {
                    muteImage.enabled = false;
                }
            }
        }
        if (gameObject.name.Equals("MusicButton")) {
            if (MusicManager.instance.isMuted) {
                if (!muteImage.enabled) {
                    muteImage.enabled = true;
                }
            } else {
                if (muteImage.enabled) {
                    muteImage.enabled = false;
                }
            }
        }
    }
}
