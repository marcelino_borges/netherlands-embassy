﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drain : MonoBehaviour
{
    public Pipe pipe;    

    // Start is called before the first frame update
    void Start()
    {
        if(pipe == null) {
            throw new MissingReferenceException("Setar no inspector a referência para o pipe dono do ralo.");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(pipe.hasWater) {
            if(!pipe.pipesManager.isGameWin) {
                pipe.pipesManager.WinGame();                
            }
        }
    }
}
