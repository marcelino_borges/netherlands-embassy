﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PipesManager : MonoBehaviour {
    public AudioClip song_part1;
    public AudioClip song_part2;
    public MusicManager musicManager;

    public GameObject[] pipesInScene;
    public List<Pipe> pipesConnectedToFaucet;
    public Pipe faucet;
    public bool isGameWin;
    public bool canPlayTheGame;
    public Animator victoryPanelAnimator, pausePanelAnimator, bgPauseAnimator;
    public GameObject pauseButton;
    public const float cooldownTimeInSeconds = .2f;

    //private bool canFollowFaucet = false;

    void Start() {
        musicManager = GameObject.FindGameObjectWithTag("musicManager").GetComponent<MusicManager>();
        SetMusic();
        canPlayTheGame = true;
        isGameWin = false;
        pipesInScene = GameObject.FindGameObjectsWithTag("pipe");
        pipesConnectedToFaucet = new List<Pipe>();
        VerifyMandatoryObjects();
        //UpdateNeighbors();
    }

    private void VerifyMandatoryObjects() {
        if (victoryPanelAnimator == null) {
            throw new MissingReferenceException("Colocar a referência do objeto do painel de vitória!");
        }

        if (pausePanelAnimator == null) {
            throw new MissingReferenceException("Colocar a referência do objeto do painel de pausa!");
        }

        if (bgPauseAnimator == null) {
            throw new MissingReferenceException("Colocar a referência do objeto do painel de background do pause!");
        }

        if (faucet == null) {
            throw new MissingReferenceException("Colocar a referência do objeto FAUCET na cena.");
        }
    }

    private void SetMusic() {
        song_part1 = Resources.Load<AudioClip>("Sounds/Songs/pipes1");
        song_part2 = Resources.Load<AudioClip>("Sounds/Songs/pipes2");

        if (musicManager != null) {
            musicManager.SetSongs(new AudioClip[] { song_part1, song_part2 });
        }
    }

    internal void WinGame() {
        UpdateNeighbors();
        canPlayTheGame = false;
        OrderListOfConnectedPipes();
        isGameWin = true;
        PaintPipes();
        SoundManager.PlaySound("waterFlow");
        Invoke("CallVictoryPanel", 1.5f);
    }

    public void PaintPipes() { 
        PaintFilledPipesFromFaucet();
    }

    public void UpdateNeighbors() {
        pipesConnectedToFaucet.Clear();

        foreach (GameObject pipeObject in pipesInScene) {
            Pipe pipe = pipeObject.GetComponent<Pipe>();
            pipe.hasWater = false;
            pipe.GetNeighbors();
        }
        StartCoroutine(SweepFromFaucet());
    }

    public IEnumerator SweepFromFaucet() {
        canPlayTheGame = false;
        faucet.FollowFaucetConnections(false);
        yield return new WaitForSeconds(cooldownTimeInSeconds);
        canPlayTheGame = true;
    }

    public void PaintFilledPipesFromFaucet() {
        pipesConnectedToFaucet.Clear();
        faucet.FollowFaucetConnections(true);
    }

    void CallVictoryPanel() {
        SoundManager.PlaySound("victory1");
        pauseButton.SetActive(false);
        victoryPanelAnimator.gameObject.SetActive(true);
        victoryPanelAnimator.Play("entry");
    }

    public void PauseGame() {
        pauseButton.SetActive(false);
        canPlayTheGame = false;
        pausePanelAnimator.gameObject.SetActive(true);
        pausePanelAnimator.SetFloat("speed", 1f);
        pausePanelAnimator.Play("entry");

        bgPauseAnimator.gameObject.SetActive(true);
        bgPauseAnimator.SetFloat("speed", 1f);
        bgPauseAnimator.Play("fade");
    }

    public void UnpauseGame() {
        canPlayTheGame = true;
        pausePanelAnimator.SetFloat("speed", -1f);
        pausePanelAnimator.Play("entry");
        Invoke("HidePausePanel", .3f);
        pauseButton.SetActive(true);
    }

    void HidePausePanel() {
        bgPauseAnimator.gameObject.SetActive(false);
        bgPauseAnimator.SetFloat("speed", -1f);
        bgPauseAnimator.Play("fade");

        pausePanelAnimator.gameObject.SetActive(false);
    }

    public void LoadLevelSelection() {
        SceneManager.LoadScene(1);
    }

    public void OrderListOfConnectedPipes() {
        pipesConnectedToFaucet.Sort(SortPipesByXPosition);
    }

    public int SortPipesByXPosition(Pipe pipe1, Pipe pipe2) {
        return (pipe1.gameObject.transform.position.x).CompareTo(pipe2.gameObject.transform.position.x);
    }
}
