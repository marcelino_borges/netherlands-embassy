﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Connector : MonoBehaviour
{
    //This will only be true and touching another connector
    public Pipe connectedPipe;
    public Pipe ownerPipe;

    void Start() {
        ownerPipe = GetComponentInParent<Pipe>();
        if (ownerPipe == null) {
            throw new MissingReferenceException("Setar o ownerPipe do connector. Problema atual no " + gameObject.name);
        }

        if(gameObject.tag == null || gameObject.tag.Equals("")) {
            throw new MissingReferenceException("Colocar a tag 'connector' no prefab dos connectors. Problema atual no connector do pipe " + ownerPipe.gameObject.name);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        StartCoroutine(saveConnectedPipeCoRoutine(collision));
    }

    private IEnumerator saveConnectedPipeCoRoutine(Collider2D collision) {
        yield return new WaitForSeconds(.1f);

        if (collision.gameObject.tag == "connector") {
            Pipe otherPipe = collision.gameObject.GetComponentInParent<Pipe>();
            connectedPipe = otherPipe;

            if (ownerPipe.name.Equals("Faucet")) {
                if (gameObject.name.Equals("connector (1)")) {
                    Debug.Log(ownerPipe.name + " tocou " + collision.gameObject.name + " do cano " + otherPipe.gameObject.name);
                }
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision) {
        if (collision.gameObject.tag == "connector") {
            Pipe otherPipe = collision.gameObject.GetComponentInParent<Pipe>();
            if (connectedPipe == otherPipe) {
                connectedPipe = null;
            }

            if (ownerPipe.name.Equals("Faucet")) {
                if (gameObject.name.Equals("connector (1)")) {
                    Debug.Log(ownerPipe.name + " soltou " + collision.gameObject.name + " do cano " + otherPipe.gameObject.name);
                }
            }
        }
    }
}
