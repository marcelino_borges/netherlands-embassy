﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Pipe : MonoBehaviour
{
    private const float animDuration = 0.2f;
    public PipesManager pipesManager;

    //public bool hasConnectionsClosed;
    public bool hasWater;
    public bool canRotate;
    public bool isFaucet;
    public bool isDrain;

    public Connector[] connectors;

    public List<Pipe> neighbors;

    private float currentRotationZ;

    [HideInInspector]
    public Sprite emptyPipe;
    public Sprite filledPipe;


    void Start() {

        emptyPipe = GetComponent<SpriteRenderer>().sprite;

        //FillWithWater();
        DOTween.Init(false, false, LogBehaviour.Default);

        pipesManager = GameObject.FindGameObjectWithTag("pipesmanager").GetComponent<PipesManager>();

        currentRotationZ = transform.rotation.eulerAngles.z;

        if(isFaucet) {
            hasWater = true;
            canRotate = false;
        } else if (isDrain) {
            hasWater = false;
            canRotate = false;
        }

        neighbors = new List<Pipe>();

        VerifyMandatoryReferences();
    }

    private void VerifyMandatoryReferences() {
        if (gameObject.tag == null || gameObject.tag.Equals("")) {
            throw new MissingReferenceException("Coloque a tag 'pipe' dos prefabs dos pipes. Problema atual no " + gameObject.name);
        }

        if (connectors == null || connectors.Length <= 0) {
            throw new MissingReferenceException("Setar as referências dos connectors vinculados ao pipe. Problema atual no " + gameObject.name);
        }

        if (filledPipe == null) {
            throw new MissingReferenceException("Setar a referência do cano preenchido com água.");
        }
    }

    public void FollowFaucetConnections(bool canPaint) {
        //Starts with the faucet (due to this function being called only by the faucet)    
        if (pipesManager != null) {
            if (neighbors.Count > 0) {
                foreach (Pipe neighbor in neighbors) {
                    neighbor.hasWater = true;
                    if (!pipesManager.pipesConnectedToFaucet.Contains(neighbor) && !neighbor.isFaucet) {
                        if (canPaint) {
                            Invoke("FillWithWater", 0.1f);
                        }
                        pipesManager.pipesConnectedToFaucet.Add(neighbor);
                        neighbor.FollowFaucetConnections(canPaint);
                    }
                }
            }            
        }
    }

    private void OnMouseUp() {        
        if (canRotate && pipesManager.canPlayTheGame && !pipesManager.isGameWin) {
            Rotate();
        }
    }

    public void GetNeighbors() {
        neighbors.Clear();
        foreach (Connector connector in connectors) {
            Pipe connectedPipe = connector.connectedPipe;
            if (connectedPipe != null) {
                if (!neighbors.Contains(connectedPipe) && !connectedPipe.isFaucet) {
                    neighbors.Add(connectedPipe);
                }
            }
        }
    }

    void Rotate() {
        currentRotationZ += 90;
        currentRotationZ = Mathf.Round(currentRotationZ);
        //Assuring the angle always ends with zero (after observing this behavior)
        string number = currentRotationZ.ToString();
        number = number.Remove(number.Length - 1,1) + "0";
        currentRotationZ = float.Parse(number);
        //Make the rotation
        gameObject.transform.DORotate(new Vector3(0, 0, currentRotationZ), animDuration).OnComplete(() => notifyPipeManager());
    }

    void notifyPipeManager() {
        pipesManager.UpdateNeighbors();
    }

    public void FillWithWater() {
        if (!isFaucet) {
            hasWater = true;
            //gameObject.GetComponent<SpriteRenderer>().color = new Color(0, 25, 255, 100);
            GetComponent<SpriteRenderer>().sprite = filledPipe;
        } 
    }

    public void TakeOutWater() {
        hasWater = false;
        //gameObject.GetComponent<SpriteRenderer>().color = new Color(255, 255, 255, 255);
        GetComponent<SpriteRenderer>().sprite = emptyPipe;
    }
}
